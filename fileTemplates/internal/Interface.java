#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end
#parse("File Header.java")

// ********************************
// @Author: TangTao tangtao2099@outlook.com
// @GitHub: https://github.com/tangtaoshadow
// @Website: https://www.promiselee.cn/tao
// @ZhiHu: https://www.zhihu.com/people/tang-tao-24-36
// @InterfaceName: ${NAME}
// @Note: 
// @CreateTime: ${YEAR}-${MONTH}-${DAY} ${HOUR}:${MINUTE}:00
// @UpdateTime: ${YEAR}-${MONTH}-${DAY} ${HOUR}:${MINUTE}:00
// ********************************

public interface ${NAME} {
}
